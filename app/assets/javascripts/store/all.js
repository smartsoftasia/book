// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs

//= require store/spree_frontend
//= jquery.bxslider
//= require_tree .
//= require store/spree_paypal_express





$(window).load(function(){
  $('.bxslider').bxSlider({
    auto: true,
    autoControls: false,
    controls: false,
    speed: 1000,
    pause: 6000,
    mode: 'fade', 
    pager: false,
    hideControlOnEnd: false,
    captions: false
  });
});